package com.example.api_qcm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiQcmApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiQcmApplication.class, args);
	}

}
