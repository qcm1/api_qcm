package com.example.api_qcm.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.api_qcm.models.Question;
import com.example.api_qcm.repositories.QuestionRepository;

import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;

@RestController
@RequestMapping("api")
public class QuestionController {
    @Autowired
    private QuestionRepository _questionrepository;

    @GetMapping("/questions")
    public List<Question> index(){
        return _questionrepository.findAll();
    }

    @GetMapping("/questions/{id}")
    public Question show(@PathVariable String id){
        int questionId = Integer.parseInt(id);
        return _questionrepository.findById(questionId).get();
    }

    @GetMapping("/qcms/{id}/questions")
    public List<Question> showResponse(@PathVariable @NotNull @DecimalMin("0") Number id){
        return _questionrepository.findByidqcm(id.intValue());
    }
}
