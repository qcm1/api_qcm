package com.example.api_qcm.controllers;

import java.util.List;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.api_qcm.models.Reponse;
import com.example.api_qcm.repositories.ReponseRepository;

import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;

@RestController
@RequestMapping("api")
public class ReponseController {
    @Autowired
    private ReponseRepository _reponseRepository;

    @GetMapping("/reponses")
    public List<Reponse> index(){
        return _reponseRepository.findAll();
    }

    @GetMapping("/reponses/{id}")
    public Reponse show(@PathVariable String id){
        int contactId = Integer.parseInt(id);
        return _reponseRepository.findById(contactId).get();
    }

    @GetMapping("/questions/{id}/reponses")
    public List<Reponse> showResponse(@PathVariable @NotNull @DecimalMin("0") Number id){
        return _reponseRepository.findByQuestion(id.intValue());
        

    }
}
