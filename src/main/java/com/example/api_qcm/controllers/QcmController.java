package com.example.api_qcm.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.api_qcm.models.Qcm;
import com.example.api_qcm.repositories.QcmRepository;

@RestController
@RequestMapping("api")
public class QcmController {
    @Autowired
    private QcmRepository _QcmRepository;

    @GetMapping("/qcms")
    public List<Qcm> index(){
        return _QcmRepository.findAll();
    }

    @GetMapping("/qcms/{id}")
    public Qcm show(@PathVariable String id){
        int questionId = Integer.parseInt(id);
        return _QcmRepository.findById(questionId).get();
    }
}
