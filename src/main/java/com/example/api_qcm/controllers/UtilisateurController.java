package com.example.api_qcm.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.api_qcm.models.Utilisateur;
import com.example.api_qcm.repositories.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api")
public class UtilisateurController {
   
    @Autowired
    private UtilisateurRepository _Utilisateurrepository;

    @GetMapping("/users")
    public List<Utilisateur> index(){
        return _Utilisateurrepository.findAll();
    }

    @GetMapping("/users/{id}")
    public Utilisateur show(@PathVariable String id){
        int contactId = Integer.parseInt(id);
        return _Utilisateurrepository.findById(contactId).get();
    }

}
