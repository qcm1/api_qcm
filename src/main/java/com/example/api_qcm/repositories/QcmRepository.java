package com.example.api_qcm.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.api_qcm.models.Qcm;

public interface QcmRepository extends JpaRepository<Qcm, Integer> {
    
}
