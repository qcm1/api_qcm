package com.example.api_qcm.repositories;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.api_qcm.models.Question;


@Repository
public interface QuestionRepository extends JpaRepository<Question, Integer> {
    List<Question> findByidqcm(Integer idqcm);
}