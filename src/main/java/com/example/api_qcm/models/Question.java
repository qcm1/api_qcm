package com.example.api_qcm.models;

import java.util.Objects;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Integer id;
    private String question;
    private Integer idqcm;

    public Question() {
    }

    public Question(Integer id, String question, Integer idqcm) {
        this.id = id;
        this.question = question;
        this.idqcm = idqcm;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQuestion() {
        return this.question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Integer getIdqcm() {
        return this.idqcm;
    }

    public void setIdqcm(Integer idqcm) {
        this.idqcm = idqcm;
    }

    public Question id(Integer id) {
        setId(id);
        return this;
    }

    public Question question(String question) {
        setQuestion(question);
        return this;
    }

    public Question idqcm(Integer idqcm) {
        setIdqcm(idqcm);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Question)) {
            return false;
        }
        Question question = (Question) o;
        return Objects.equals(id, question.id) && Objects.equals(question, question.question) && Objects.equals(idqcm, question.idqcm);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, question, idqcm);
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", question='" + getQuestion() + "'" +
            ", idqcm='" + getIdqcm() + "'" +
            "}";
    }



}
